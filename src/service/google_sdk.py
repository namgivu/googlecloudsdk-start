import time
from datetime import datetime

import googleapiclient.discovery


"""
demo goal - can have python code that do similar with below shell command
    # step=create snapshot         .               .                                .                      .                        .                                   && step=create disk            .                        .                  .              .                      .                       .                            step=create instance from disk       .                  .                                                .                                                                              .                            .                           .                .                        .                                                   .
    #                              from this disk  .                                gcp project+zone       .                        .                                   && .                           .                        from this snapshot .              .                      gcp project+zone        .                            .                                    new instance name  gcp project+zone                                 create instance from this disk+device name                                     .                           instance hardware            .                external ip              .                                                   .                 your service account here
    gcloud compute disks snapshot  $FROM_DISK      --snapshot-names=$SNAPSHOT_NAME  --project=$GCP_PROJECT --zone=asia-southeast1-b --storage-location=asia-southeast1  && gcloud compute disks create $DISK_NAME  --size "200" --source-snapshot  $SNAPSHOT_NAME --type "pd-standard"   --project $GCP_PROJECT  --zone "asia-southeast1-b";  gcloud beta compute instances create $INSTANCE_NAME     --project=$GCP_PROJECT --zone=asia-southeast1-b  --disk=name=$DISK_NAME,device-name=$DISK_NAME,mode=rw,boot=yes,auto-delete=yes --reservation-affinity=any  --machine-type=n1-standard-1 --subnet=default --address=$EXTERNAL_IP   --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=883462883129-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append
"""


ZONE_REF    = 'ref. https://cloud.google.com/compute/docs/regions-zones'
api_service = googleapiclient.discovery.build('compute', 'v1')


#region wait for long run
def wait_for_operation(project, zone:ZONE_REF, operation, title=None):
    """
     ref. https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/compute/api/create_instance.py#L128
     ref. https://cloud.google.com/compute/docs/tutorials/python-guide
     """
    if not title: print(title)
    print('Waiting for operation to finish... ', end='')
    while True:
        result = api_service.zoneOperations().get(
            project=project, zone=zone,
            operation=operation,
        ).execute()

        if result['status'] == 'DONE':
            print('Done')
            if 'error' in result:
                raise Exception(result['error'])
            return result

        time.sleep(1)
#endregion wait for long run


def list_instance(project, zone:ZONE_REF):
    #region result = api_service.instances().list(project=project, zone=zone).execute()
    """
    associated RESTful endpoint
    https://cloud.google.com/compute/docs/reference/rest/v1/instances/list
    """
    collection  = api_service.instances()
    method      = collection.list(project=project, zone=zone)
    result      = method.execute()
    #endregion
    return result.get('items')


#region disk
def list_disk(project, zone:ZONE_REF):
    """
    associated RESTful endpoint
    https://cloud.google.com/compute/docs/reference/rest/v1/disks/list
    """
    r = api_service.disks().list(project=project, zone=zone).execute()
    return r.get('items')


def create_disk(project, zone:ZONE_REF, name, snapshot_name):
    """
    associated RESTful endpoint
    https://cloud.google.com/compute/docs/reference/rest/v1/disks/insert

    associated command
    gcloud compute disks create $DISK_NAME  --size "200" --source-snapshot  $SNAPSHOT_NAME --type "pd-standard"   --project $GCP_PROJECT  --zone "asia-southeast1-b"
    """
    operation = api_service.disks().insert(
        project=project, zone=zone,
        body = {
            'name'             : name,
            'sizeGb'           : 200,
            'sourceSnapshotId' : snapshot_name,
            'type'             : f'projects/{project}/zones/{zone}/diskTypes/pd-standard',
        },
    ).execute()
    wait_for_operation(project, zone, operation['name'])


def delete_disk(project, zone:ZONE_REF, disk_name):
    """
    associated RESTful endpoint
    https://cloud.google.com/compute/docs/reference/rest/v1/disks/delete
    """
    api_service.disks().delete(
        project=project,zone=zone,
        disk=disk_name,
    ).execute()

#endregion disk


#region snapshot
def list_snapshot(project):
    """
    associated RESTful endpoint
    https://cloud.google.com/compute/docs/reference/rest/v1/snapshots/list
    """
    r = api_service.snapshots().list(project=project).execute()
    return r.get('items')


def create_snapshot(project, zone:ZONE_REF, disk:'disk to snapshot', snapshot_name):
    """
    associated RESTful endpoint
    https://cloud.google.com/compute/docs/reference/rest/v1/snapshots/delete
    TODO report to GCP team this doc has outofdate :resourceId param - the trythisapi on the right gives the right name :disk

    associated shell command
    gcloud compute disks snapshot  $FROM_DISK  --snapshot-names=$SNAPSHOT_NAME  --project=$GCP_PROJECT  --zone=asia-southeast1-b  --storage-location=asia-southeast1
    """
    operation = api_service.disks().createSnapshot(
        project=project, zone=zone,
        disk=disk,       body={'name': snapshot_name},
    ).execute()

    wait_for_operation(project, zone, operation['name'])


def delete_snapshot(project, zone:ZONE_REF, snapshot_name):
    """
    associated RESTful endpoint
    https://cloud.google.com/compute/docs/reference/rest/v1/disks/createSnapshot
    """
    """
    associated RESTful endpoint
    https://cloud.google.com/compute/docs/reference/rest/v1/snapshots/delete
    TODO report to GCP team this doc has outofdate :resourceId param - the trythisapi on the right gives the right name :disk
    """
    api_service.snapshots().delete(
        project=project,
        snapshot=snapshot_name,
    ).execute()

#endregion snapshot


#region instance
def delete_instance(project, zone:ZONE_REF, instance_name):
    """
    associated RESTful endpoint
    https://cloud.google.com/compute/docs/reference/rest/v1/instances/delete
    """
    operation = api_service.instances().delete(
        project=project,zone=zone,
        instance=instance_name,
    ).execute()
    wait_for_operation(project, zone, operation['name'])


def clone_instance(project, zone:ZONE_REF, from_disk, to_instance):
    ts = datetime.today().strftime('%Y%m%d-%H%M%S')
    snapshot_name = f'testsnapshot-{from_disk}-{ts}' ; create_snapshot(project, zone, from_disk, snapshot_name)
    disk_name     = f'testsnapshot-{to_instance}-{ts}'

    #region create instance from snapshot/disk
    '''
    associated api endpoint
    https://cloud.google.com/compute/docs/reference/rest/v1/instances/insert
    
    associated command
    gcloud beta compute instances  create $CLONED_TO_INSTANCE  --project=$GCP_PROJECT --zone=asia-southeast1-b  --disk=name=$DISK_NAME,device-name=$DISK_NAME,mode=rw,boot=yes,auto-delete=yes --reservation-affinity=any  --machine-type=n1-standard-1 --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=883462883129-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append
    #           .       .          .      name                 .                      .                         .      diskName        deviceName             .       .        .               ?                           machineType                  .                .                      ?                            ?                                                                    ?
    '''
    region = zone[:-2]
    operation = api_service.instances().insert(
        project=project, zone=zone,
        body = {
            'name': to_instance,

            'disks' : [{
                'initializeParams': {
                    'diskName': disk_name,
                    'sourceSnapshot': f'global/snapshots/{snapshot_name}',
                },
                'deviceName' : disk_name,
                'mode'       : 'READ_WRITE',
                'boot'       : True,
                "autoDelete" : True,
            }],

            'machineType' : f'zones/{zone}/machineTypes/n1-standard-1',

            'networkInterfaces': [{
                'subnetwork': f'https://www.googleapis.com/compute/v1/projects/{project}/regions/{region}/subnetworks/default',
                'accessConfigs': [{
                    'networkTier': 'PREMIUM',
                    # 'natIP': 'TODO external ip here',
                }]
            }]
        },
    ).execute()

    wait_for_operation(project, zone, operation['name'])
    #endregion

    return snapshot_name

#endregion instance
