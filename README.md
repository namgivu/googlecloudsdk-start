NOTE: recommended to use command line version - look for "Equivalent REST or command line" at end your GCP console page

everything start [here](https://cloud.google.com/compute/docs/tutorials/python-guide)

# install gg cloud sdk 
ref. https://cloud.google.com/sdk/docs/quickstart-debian-ubuntu
```bash
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list  # Add the Cloud SDK distribution URI as a package source
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -  # Import the Google Cloud Platform public key
sudo apt-get update && sudo apt-get install -y google-cloud-sdk  # Update the package list and install the Cloud SDK
    yes | gcloud components update

    # optional 
    gcloud config set accessibility/screen_reader true
```

some go-next command
```bash 
gcloud init
gcloud config list
gcloud info
```

# working with python client WIP
enable api compute engine api [ref](https://console.cloud.google.com/apis/library/compute.googleapis.com?q=compute&id=a08439d8-80d6-43f1-af2e-6878251f018d&project=gigacover&supportedpurview=project)

install python client
```bash 
cd /tmp
    pipenv install google-api-python-client
```

do auth
```bash
gcloud auth application-default login
```

must understand 'service object' basis to continue
[ref](https://github.com/googleapis/google-api-python-client/blob/master/docs/start.md#building-and-calling-a-service)
> Each :API service provides access to :resources
> resources of the same type is called a :collection
> :service-object is constructed with a function for every :collection

multiple sample code 
ref00. https://www.programcreek.com/python/example/101769/googleapiclient.discovery.build
ref01. https://cloud.google.com/compute/docs/tutorials/python-guide
ref02. https://github.com/GoogleCloudPlatform/python-docs-samples/tree/master/compute/api
