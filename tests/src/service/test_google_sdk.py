import os
from datetime import datetime

from dotenv import load_dotenv
from pprint import pprint

import src.service.google_sdk  as Gcsdk  # Gcsdk aka google cloud sdk


#region load GCP_PROJECT from .env
load_dotenv()

def get_n_validate(envvar_key):
    v = os.environ.get(envvar_key)
    if v is None: raise Exception(f'Envvar :{envvar_key} is required in .env')
    return v

GCP_PROJECT        = get_n_validate('GCP_PROJECT')
GCP_ZONE           = get_n_validate('GCP_ZONE')
DEMO_DISK_NAME     = get_n_validate('DEMO_DISK_NAME')
CLONED_TO_INSTANCE = get_n_validate('CLONED_TO_INSTANCE')
#endregion


def test_list_instance():
    ACT = Gcsdk.list_instance(project=GCP_PROJECT, zone=GCP_ZONE)
    assert ACT

    print('')
    ACT_name        = [i['name'] for i in ACT]                                              ; print(ACT_name)
    ACT_internal_ip = [i['networkInterfaces'][0]['networkIP'] for i in ACT]                 ; print(ACT_internal_ip)
    ACT_external_ip = [i['networkInterfaces'][0]['accessConfigs'][0]['natIP'] for i in ACT] ; print(ACT_external_ip)

    ACT_summary = []
    for i, name in enumerate(ACT_name):
        d = {
            'name'        : name,
            'internal_ip' : ACT_internal_ip[i],
            'external_ip' : ACT_external_ip[i],
        }
        ACT_summary.append(d)
    pprint(ACT_summary)


#region disk
def test_list_disk():
    ACT = Gcsdk.list_disk(project=GCP_PROJECT, zone=GCP_ZONE)
    assert ACT

    print('')
    ACT_name = [i['name'] for i in ACT] ; print(ACT_name)

    EXP_disk = DEMO_DISK_NAME
    assert EXP_disk in ACT_name


def test_createdelete_disk():
    ts = datetime.today().strftime('%Y%m%d-%H%M%S')

    snapshot_name = f'testsnapshot-{DEMO_DISK_NAME}-{ts}' ; Gcsdk.create_snapshot(project=GCP_PROJECT, zone=GCP_ZONE, disk=DEMO_DISK_NAME, snapshot_name=snapshot_name)
    disk_name     = f'testdisk-{DEMO_DISK_NAME}-{ts}'     ;     Gcsdk.create_disk(project=GCP_PROJECT, zone=GCP_ZONE, name=disk_name,      snapshot_name=snapshot_name)

    #TODO remove this cleanup after tested
    Gcsdk.delete_disk(project=GCP_PROJECT, zone=GCP_ZONE, disk_name=disk_name)
    Gcsdk.delete_snapshot(project=GCP_PROJECT, zone=GCP_ZONE, snapshot_name=snapshot_name)

#endregion disk


#region snapshot
def test_list_snapshot():
    ACT = Gcsdk.list_snapshot(project=GCP_PROJECT)
    assert ACT

    print('')
    ACT_name = [i['name'] for i in ACT] ; print(ACT_name)


def test_create_snapshot():
    ts = datetime.today().strftime('%Y%m%d-%H%M%S')
    snapshot_name = f'testsnapshot-{DEMO_DISK_NAME}-{ts}'

    Gcsdk.create_snapshot(
        project=GCP_PROJECT, zone=GCP_ZONE,
        disk=DEMO_DISK_NAME, snapshot_name=snapshot_name,
    )

    l=Gcsdk.list_snapshot(project=GCP_PROJECT); ACT_snapshot_list=[ i['name'] for i in l]
    assert snapshot_name in ACT_snapshot_list

    # clean up the test-created snapshot
    Gcsdk.delete_snapshot(
        project=GCP_PROJECT, zone=GCP_ZONE,
        snapshot_name=snapshot_name,
    )

#endregion snapshot


def test_clone_instance():
    snapshot_name = Gcsdk.clone_instance(
        project=GCP_PROJECT,      zone=GCP_ZONE,
        from_disk=DEMO_DISK_NAME, to_instance=CLONED_TO_INSTANCE,
    )

    # cleanup after tested
    Gcsdk.delete_instance(project=GCP_PROJECT, zone=GCP_ZONE, instance_name=CLONED_TO_INSTANCE)
    Gcsdk.delete_snapshot(project=GCP_PROJECT, zone=GCP_ZONE, snapshot_name=snapshot_name)
